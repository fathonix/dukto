/* DUKTO - A simple, fast and multi-platform file transfer tool for LAN users
 * Copyright (C) 2011 Emanuele Colombo
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

import QtQuick 2.3

Item {
    Flickable {
        id: flickableText
        anchors.fill: parent
        anchors.margins: 5
        flickableDirection: Flickable.VerticalFlick
        clip: true
        Rectangle {
            x: 27
            y: 15
            width: 64
            height: 64
            color: theme.color2
            Image {
                source: "TileGradient.png"
                anchors.fill: parent
            }
            Image {
                source: "DuktoMetroIcon.png"
                anchors.fill: parent
            }
        }
        SmoothText {
            y: 80
            x: 22
            font.pixelSize: 80
            text: "Dukto R6 Android"
            color: "#555555"
        }
        SmoothText {
            x: 25
            y: 70 + 75
            font.pixelSize: 38
            text: "Created by Emanuele Colombo.<br>Forked to QT 6<br>by Xu Zhen & maz-1<br>Unix, GNU/Linux, Android,<br>Windows, Ubuntu Touch & Symbian<br>Port by Kafabih"
            color: "#888888"
        }
        SmoothText {
            x: 25
            y: 220 + 75
            font.pixelSize: 32
            text: "Website: <a href=\"https://gitlab.com/kafabih-kr/dukto/\">https://gitlab.com/kafabih-kr/dukto/</a>"
            onLinkActivated: Qt.openUrlExternally(link)
            color: "#888888"
        }

        SText {
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.rightMargin: 40
            anchors.leftMargin: 25
            font.pixelSize: 12
            y: 320
            color: "#888888"
            wrapMode: "WordWrap"
            text: "This application and it's source code are released free and open source project by using the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html\">GNU GPLv2 Licensed</a>.<br>Thank's to two Chineese guys named Xu Zhen and maz-1 (github name). Also won't forgot thank's to Kafabih for porting this application to Android, Unix, GNU/Linux, <a href=\"https://gitlab.com/kafabih-kr/dukto/-/releases/\">Windows</a> and additional Symbian OS.Dukto also available on <a href=\"https://apps.apple.com/us/app/dukto/id588663167/\">iOS / iPadOS</a> <a href=\"https://apps.apple.com/us/app/dukto-pro/id588657099\">(Pro Version)</a>, <a href=\"https://kafabih-kr.gitlab.io/kr-droid-app-collections/\">Android</a>, of course the <a href=\"https://snapcraft.io/dukto/\">Snap Store</a> and <a href=\"https://gitlab.com/kafabih-kr/dukto/-/releases/\">Flatpak</a> for future GNU/Linux Packaging. Give Kafabih support for made and maintenance more awesomes apps (in Indonesian Rupiah's) by scanning QR Code at below or by click <a href=\"https://saweria.co/kafnix/\">here</a>.
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }
}


